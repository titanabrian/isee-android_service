package titan.tracklite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    String server;
    EditText et_email;
    EditText et_password;
    Button btn_login;
    String email;
    String password;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = et_email.getText().toString();
                password = et_password.getText().toString();
                server = "http://192.168.1.9/godeye/login.php?email="+email+"&password="+password;
                if(email != null && password!=null){
                    login();
                }
            }
        });
    }

    public void login(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if(status){
                                JSONObject user = response.getJSONObject("user");
                                email = user.getString("email");
                                saveSession(email);
                                Toast t = Toast.makeText(LoginActivity.this,"Login Berhasil",Toast.LENGTH_SHORT);
                                t.show();
                                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(i);
                            }else{
                                Toast t = Toast.makeText(LoginActivity.this,"Username atau Password Salah",Toast.LENGTH_SHORT);
                                t.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast t = Toast.makeText(LoginActivity.this,"Exception",Toast.LENGTH_SHORT);
                            t.show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        );
        MySingleton.getmInstance(this).addRequest(jsonObjectRequest);
    }

    public void saveSession(String mEmail){
        SharedPreferences session = getSharedPreferences("session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = session.edit();
        editor.putString("email",mEmail);
        editor.apply();
    }
}

